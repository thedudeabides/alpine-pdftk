## Split PDFs

```sh
docker run --rm -v "${PWD}":/files registry.gitlab.com/thedudeabides/alpine-pdftk filename.pdf burst
```